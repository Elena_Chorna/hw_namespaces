<section class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>Your cart</h2>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-1">
            </div>
            <div class="col-md-3">
                <h3>Product name</h3>
            </div>
            <div class="col-md-2">
                <h3>Amount</h3>
            </div>
            <div class="col-md-2">
                <h3>Price (USD)</h3>
            </div>
        </div>

        <?php
        $arr_total_price = array();
        foreach ($show_cart as $key => $value):
            try{
                $sql = 'SELECT * FROM product WHERE id="'.$key.'"';
                $result = $pdo->query($sql);

            }catch(PDOException $e){
                echo "Error: ".$e->getMessage();
                exit();
            }
            $product = $result->fetch(); ?>
            <div class="row cart-block">
                <div class="col-md-1">
                    <img src="img/<?= $product['imgsrc']; ?>">
                </div>
                <div class="col-md-3">
                    <h4><?= $product['productname']; ?></h4>
                </div>
                <div class="col-md-2">
                    <p><?= $value ;?></p>
                </div>
                <div class="col-md-2">
                    </p><?php echo $product['productprice']*$value; ?></p>
                </div>
            </div>
        <?php
            $arr_total_price[] = $product['productprice']*$value;
        endforeach;?>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p>Total amount: <b><?= $Cart::countItems();?></b></p>
                <p>Total price in USD: <b><?= getTotalSum($arr_total_price); ?></b></p>
                <p>Total price in UAH: <b><?= $Exchange->getChange(getTotalSum($arr_total_price), 27.0); ?></b></p>
            </div>
        </div>
    </div>
</section>




