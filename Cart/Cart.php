<?php
namespace Cart;

class Cart
{
    protected $products = array();

    /*
     * Get all products
     */
    public function getProducts(){
        $cookie = isset($_COOKIE['cart']) ? $_COOKIE['cart'] : "";
        $cookie = stripslashes($cookie);
        $cart = json_decode($cookie, true);
        $productsInCart = $this->products;
        return $productsInCart = $cart;
    }
    /*
     * Count items in cart when user add new product
     */
    public static function countItems()
    {
        $cookie = $_COOKIE['cart'];
        if (isset($cookie)) {
            $cookie = stripslashes($cookie);
            $cart = json_decode($cookie, true);
            $count = 0;
            foreach ($cart as $id => $quantity) {
                $count = $count + $quantity;
            }
            return $count;
        } else {
            return 0;
        }
    }

    /*
     * Set cookie
     */
    public function saveCart($id){
        $cookie = isset($_COOKIE['cart']) ? $_COOKIE['cart'] : "";
        $cookie = stripslashes($cookie);
        $cart = json_decode($cookie, true);
        $productsInCart = $this->products;
        if(!$cart){
            $cart = array();
        }else{
            $productsInCart = $cart;
        }

        if(array_key_exists($id, $productsInCart)){
            $productsInCart[$id] ++;
        }else{
            $productsInCart[$id] = '1';
        }
        return setcookie('cart', json_encode($productsInCart), time() + 60 * 60 * 2);
    }

    public function __construct(){
        $cookie = $_COOKIE['cart'];

        if (isset($cookie)) {
            $cart = json_decode($cookie, true); //преобразуем куки в массив
            foreach ($cart as $product) {//Преобразовал индексы массива в отдельные массивы и по очереди добавил в свойство
                $this->products[] = $product;
            }
        }
    }
}