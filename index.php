<?php
require_once 'functions.php';
use Cart\Cart as Cart;
$Cart = new Cart();

require_once 'header.php';
require_once 'db_connect.php';

try{
    $sql = 'SELECT * FROM product';
    $result = $pdo -> query($sql);

}catch(PDOException $e){
    echo "Error in getting hotel data: ".$e -> getMessage();
    exit();
}

include_once('products_show.html.php');

require_once 'footer.php';

