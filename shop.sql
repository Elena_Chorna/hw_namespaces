-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 01 2017 г., 14:25
-- Версия сервера: 5.5.53
-- Версия PHP: 7.0.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `shop`
--

-- --------------------------------------------------------

--
-- Структура таблицы `order`
--

CREATE TABLE `order` (
  `id` int(11) NOT NULL,
  `fullname` text,
  `phone` int(11) DEFAULT NULL,
  `email` text,
  `productid` int(11) DEFAULT NULL,
  `productammount` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order`
--

INSERT INTO `order` (`id`, `fullname`, `phone`, `email`, `productid`, `productammount`) VALUES
(1, 'Разное', 111, 'frolova8888@mail.ru', 1, 4),
(2, 'Елена', 12345678, 'lovafro@gmail.com', 1, 5),
(3, 'i', 2147483647, 'lovafro@gmail.com', 4, 3),
(4, 'Елена', 2147483647, 'frolova8888@mail.ru', 4, 3),
(5, 'Разное', 12345678, 'lovafro@gmail.com', 3, 1),
(6, 'Елена', 2147483647, 'lovafro@gmail.com', 2, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE `product` (
  `id` int(11) NOT NULL,
  `productname` text,
  `productprice` float NOT NULL,
  `productsdesc` text,
  `imgsrc` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `productname`, `productprice`, `productsdesc`, `imgsrc`) VALUES
(1, 'Mountains', 100, 'The print only option is the most affordable route to go. It is just what it sounds like: prints on high quality archival photographic paper without any mounting or framing. One of the benefits of going this route is that you can bring them to your local frame shop to have the matted and framed exactly as you like. Prints are also great if you are in a hurry as they usually arrive in 5-7 days from the time of order.', 'img_1.jpg'),
(2, 'JUNE RISE', 120, 'This mounting option is by far our most popular format. It is both durable and beautiful. It allows the images to reveal an almost three dimensional quality creating the impression of actually being there. This is because there is almost no glare or reflection such as you find with even the most expensive types of glass. An additional benefit is that the artwork is very durable and can even be cleaned with a soft damp cloth. The artwork also resists fading because it has UV protection. When you order a plaque from us, they arrive ready to hang.', 'img_2.jpg'),
(3, 'Sea view', 85, 'In felis felis, convallis vitae magna et, facilisis venenatis nunc. Vestibulum scelerisque viverra ligula a dictum. Fusce metus sapien, maximus a ligula et, luctus feugiat sem. Suspendisse nec ante pretium ex blandit fermentum a quis enim. Etiam non vestibulum erat. Quisque quis luctus massa. Nullam interdum vulputate ligula, vel vulputate ex.', 'img3.jpg'),
(4, 'Ocean mirror', 115, 'Nulla et felis cursus, dapibus neque sagittis, imperdiet metus. Praesent mauris metus, placerat sit amet nibh eu, sodales iaculis augue. Phasellus porta et risus eget pellentesque. Aenean convallis consequat dolor, ornare auctor quam fringilla vitae. Morbi maximus finibus rutrum. Fusce blandit lectus nibh, non scelerisque velit luctus vel.', 'img4.jpg'),
(5, 'Green wood', 105, 'This is because there is almost no glare or reflection such as you find with even the most expensive types of glass. An additional benefit is that the artwork is very durable and can even be cleaned with a soft damp cloth. The artwork also resists fading because it has UV protection. When you order a plaque from us, they arrive ready to hang.', 'img5.jpg'),
(6, 'Safari', 155, 'Praesent mauris metus, placerat sit amet nibh eu, sodales iaculis augue. Phasellus porta et risus eget pellentesque. Aenean convallis consequat dolor, ornare auctor quam fringilla vitae. Morbi maximus finibus rutrum. Fusce blandit lectus nibh, non scelerisque velit luctus vel.', 'img6.jpg');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
