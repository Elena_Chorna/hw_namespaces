<section class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h2>All Products</h2>
            </div>
        </div>
        <div class="row align-items-end">
            <?php foreach($result as $product): ?>
                <div class="col-md-4 product-block height-550">
                    <a href="product.php?id=<?=$product['id']?>"><img src="img/<?=$product['imgsrc']?>"></a>
                    <h4> <a href="product.php?id=<?=$product['id']?>"><?=$product['productname']?></a></h4>
                    <h5><?=$product['productprice']?>, USD</h5>
                    <p><?=$product['productsdesc']?></p>
                    <a class="buy-now" href="product_add_cart.php?id=<?=$product['id']?>">Add to cart</a>
                </div>
            <?php endforeach;?>
        </div>
    </div>
</section>




