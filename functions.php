<?php
//Get total sum in cart
function getTotalSum($arr){
    $sum = '';
    foreach ($arr as $item) {
        $sum += $item;
    }
    return $sum;
}

//Autoload
spl_autoload_register ('autoload');
function autoload ($className) {
    $fileName = $className . '.php';
    include  str_replace('\\','/',$fileName);
}


