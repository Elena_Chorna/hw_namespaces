<?php
require_once 'functions.php';
use Cart\Cart as Cart;
use Cart\Exchange\ExChange as Exchange;
$Cart = new Cart();
$Exchange = new Exchange();


require_once 'header.php';
require_once 'db_connect.php';

if (!isset($_COOKIE['cart'])) {
    $show_cart =  array();
}
$show_cart = $Cart->getProducts();


include 'cart_show.html.php';
require_once 'footer.php';

